<?php

namespace Kowal\Email\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;

class Send extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;

    public function __construct(
        Context          $context,
        StateInterface   $inlineTranslation,
        Escaper          $escaper,
        TransportBuilder $transportBuilder
    )
    {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
    }

    public function email( $message = '...', $domain = '', $to = 'kontakt@kowal.co', $sender_name = 'Admin', $sender_email = 'kontakt@kowal.co')
    {
        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $sender_name,
                'email' => $sender_email,
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('email_error_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'message' => $message,
                    'domain' => $domain,
                ])
                ->setFrom($sender)
                ->addTo($to)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}